# ---- Hack ----
# Fix bug in grappelli from source
import nested_inlines.helpers
import grappelli_nested.helpers
grappelli_nested.helpers.AdminErrorList = nested_inlines.helpers.AdminErrorList
nested_inlines.helpers.InlineAdminFormSet = grappelli_nested.helpers.InlineAdminFormSet
# --------------
#Fix bug
import grappelli_nested.admin

from django.conf import settings


def add_nested_inline_formsets(self, request, inline, formset, depth=0):
    if depth > 5:
        raise Exception("Maximum nesting depth reached (5)")
    empty_form = formset.empty_form
    for form in formset.forms + [empty_form]:
        nested_formsets = []
        for nested_inline in inline.get_inline_instances(request):
            InlineFormSet = nested_inline.get_formset(request, form.instance)
            prefix = "%s-%s" % (form.prefix, InlineFormSet.get_default_prefix())
            #because of form nesting with extra=0 it might happen, that the post data doesn't include values for the formset.
            #This would lead to a Exception, because the ManagementForm construction fails. So we check if there is data available, and otherwise create an empty form
            keys = request.POST.keys()
            has_params = any(s.startswith(prefix) for s in keys)
            if request.method == 'POST' and has_params:
                nested_formset = InlineFormSet(
                    request.POST,
                    request.FILES,
                    save_as_new="_saveasnew" in request.POST,
                    instance=form.instance,
                    prefix=prefix,
                    queryset=nested_inline.queryset(request)
                )
            else:
                nested_formset = InlineFormSet(instance=form.instance,
                                               prefix=prefix, queryset=nested_inline.queryset(request))
            nested_formsets.append(nested_formset)
            if nested_inline.inlines:
                self.add_nested_inline_formsets(request, nested_inline, nested_formset, depth=depth+1)
        form.nested_formsets = nested_formsets
    form = empty_form
    media = None
    def get_media(extra_media):
        if media:
            return media + extra_media
        else:
            return extra_media
    wrapped_nested_formsets = []
    for nested_inline, nested_formset in zip(inline.get_inline_instances(request), form.nested_formsets):
         if form.instance.pk:
                instance = form.instance
         else:
            instance = None
         fieldsets = list(nested_inline.get_fieldsets(request))
         readonly = list(nested_inline.get_readonly_fields(request))
         prepopulated = dict(nested_inline.get_prepopulated_fields(request))
         wrapped_nested_formset = grappelli_nested.admin.InlineAdminFormSet(nested_inline, nested_formset,
             fieldsets, prepopulated, readonly, model_admin=self)
         wrapped_nested_formsets.append(wrapped_nested_formset)
         media = get_media(wrapped_nested_formset.media)
         if nested_inline.inlines:
             media = get_media(self.wrap_nested_inline_formsets(request, nested_inline, nested_formset))
    form.nested_formsets = wrapped_nested_formsets
    formset.__class__.empty_form = empty_form
grappelli_nested.admin.NestedModelAdmin.add_nested_inline_formsets = add_nested_inline_formsets
#-------------------
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class Race(models.Model):
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)
    description = models.TextField(_("Description"), blank=True)
    avg_weight = models.DecimalField(_("Average weight"), decimal_places=2, max_digits=20, null=True, blank=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('race')
        verbose_name_plural = _('races')

    @property
    def in_stock(self):
        return self.stocks.order_by('-date').first()


class PlantStock(models.Model):
    race = models.ForeignKey(Race, related_name='stock', null=False, blank=False)
    date = models.DateField(_("Date"), default=timezone.now)
    amount = models.FloatField(_("Meters of plants"), blank=True)

    class Meta:
        verbose_name = _('plant stock')
        verbose_name_plural = _("plant stock")


class Container(models.Model):
    """ Other name: Colli - it's stacked on pallets and contain packages
    """
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)
    description = models.TextField(_("Description"), blank=True)
    weight = models.DecimalField(_("Weight"), decimal_places=2, max_digits=22)
    per_pallet = models.PositiveIntegerField(_("Per pallet"), blank=False, null=False)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _('container')
        verbose_name_plural = _("containers")

    @property
    def in_stock(self):
        return self.stocks.order_by('-date').first()


class ContainerStock(models.Model):
    container = models.ForeignKey(Container, null=False, blank=False, related_name='stock')
    date = models.DateField(_("Date"), default=timezone.now)
    amount = models.PositiveIntegerField(_("Amount"), blank=True, null=True)

    class Meta:
        verbose_name = _("container stock")
        verbose_name_plural = _("container stock")


class Package(models.Model):
    """ Package is contained by container
    Special one is: no package (when only Container is used)
    """
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)
    description = models.TextField(_("Description"), blank=True)
    weight = models.DecimalField(_("Weight"), decimal_places=2, max_digits=22)
    container = models.ManyToManyField(Container, through='PackageAmount')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("package")
        verbose_name_plural = _("packages")

    @property
    def in_stock(self):
        return self.stocks.order_by('-date').first()


class PackageStock(models.Model):
    package = models.ForeignKey(Package, null=False, blank=False, related_name='stock')
    date = models.DateField(_("Date"), default=timezone.now)
    amount = models.PositiveIntegerField(_("Amount"), blank=True, null=True)

    class Meta:
        verbose_name = _("package stock")
        verbose_name_plural = _("package stock")


class PackageAmount(models.Model):
    """ Stores amount of packages that fits into Container
    """
    package = models.ForeignKey(Package, related_name="packages_amount")
    container = models.ForeignKey(Container, related_name="packages_amount")
    amount = models.PositiveIntegerField(_("Amount"), default=1)

    class Meta:
        verbose_name = _("package amount")
        verbose_name_plural = _("package amount")

    @property
    def weight(self):
        return self.container.weight + self.amount * self.package.weight

    def __unicode__(self):
        return _(u"%(package)s in %(container)s") % {
            'package': self.package.name,
            'container': self.container.name,
        }


class TradeClass(models.Model):
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)

    class Meta:
        verbose_name = _("trade class")
        verbose_name_plural = _("trade class")

    def __unicode__(self):
        return self.name


class Product(models.Model):
    #!! article_number
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)
    description = models.TextField(_("Description"), blank=True)
    trade_class = models.ForeignKey(TradeClass, related_name='+')
    races = models.ManyToManyField(Race, through='ProductRaceConfig', related_name='products')
    packaging = models.ForeignKey(PackageAmount, null=False, blank=False, related_name='products')
    assumed_weight = models.DecimalField(_("Assumed weight"), decimal_places=2, max_digits=22)
    real_weight = models.DecimalField(_("Real weight"), decimal_places=2, max_digits=22)
    order = models.IntegerField(_("Product order"), blank=True, null=True)
    #is_active = models.BooleanField("is_active")

    class Meta:
        verbose_name = _("product")
        verbose_name_plural = _("products")
        ordering = ('order', 'name')

    def __unicode__(self):
        return _(u"%(name)s") % {
            'name': self.name,
            #'package': self.packaging.package.name,
            #'container': self.packaging.container.name,
            #'real_weight': self.real_weight,
        }

    @staticmethod
    def autocomplete_search_fields():
        return "name__icontains",


class ProductRaceConfig(models.Model):
    product = models.ForeignKey(Product, related_name='races_config', null=False, blank=False)
    race = models.ForeignKey(Race, related_name='+', null=False, blank=False)
    percent = models.DecimalField(_("Percent"), decimal_places=4, max_digits=7)

    class Meta:
        verbose_name = _("product race config")
        verbose_name_plural = _("product race config")


class Customer(models.Model):
    #!! customer_number
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)
    contact_person = models.CharField(_("Contact Person"), max_length=250, blank=True)
    is_active = models.BooleanField(_("Active"), default=True)
    notes = models.TextField(_("Notes"), blank=True)
    # Address
    country = models.CharField(_("Country"), max_length=100, null=False, blank=False)
    city = models.CharField(_("City"), max_length=100, null=False, blank=False)
    street = models.CharField(_("Street"), max_length=100, null=False, blank=True)
    street_number = models.CharField(_("Street Number"), max_length=10, null=False, blank=True)
    flat_number = models.CharField(_("Flat Number"), max_length=10, null=False, blank=True)
    zip = models.CharField(_("Postal code"), max_length=15, null=False, blank=True)
    address_notes = models.TextField("Address notes")

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("customer")
        verbose_name_plural = _("customers")

    @staticmethod
    def autocomplete_search_fields():
        return "name__icontains",


class CustomerDistributionCenter(models.Model):
    customer = models.ForeignKey(Customer, null=False, blank=False)
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)
    is_active = models.BooleanField(_("Active"), default=True)
    notes = models.TextField(_("Notes"), blank=True)
    # Address
    country = models.CharField(_("Country"), max_length=100, null=False, blank=False)
    city = models.CharField(_("City"), max_length=100, null=False, blank=False)
    street = models.CharField(_("Street"), max_length=100, null=False, blank=True)
    street_number = models.CharField(_("Street Number"), max_length=10, null=False, blank=True)
    flat_number = models.CharField(_("Flat Number"), max_length=10, null=False, blank=True)
    zip = models.CharField(_("Postal code"), max_length=15, null=False, blank=True)
    address_notes = models.TextField("Address notes")
    default_products = models.ManyToManyField(Product, _("Default Products"), null=True, related_name='default_for')

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = _("customer distribution center")
        verbose_name_plural = _("customer distribution center")

    @staticmethod
    def autocomplete_search_fields():
        return "name__icontains",


class PricePeriod(models.Model):
    customer = models.ForeignKey(Customer, null=False, blank=False, related_name="prices")
    date_from = models.DateField(_("From date"), default=timezone.now)
    date_to = models.DateField(_("To date"), default=None, blank=True, null=True)

    def __unicode__(self):
        count = 0
        if self.id:
            count = self.prices.count()
        return "from %s to %s (%s)" % (self.date_from, self.date_to, count)

    class Meta:
        verbose_name = _("price period")
        verbose_name_plural = _("price periods")


class Price(models.Model):
    product = models.ForeignKey(Product, null=False, blank=False, related_name='prices')
    period = models.ForeignKey(PricePeriod, null=True, blank=False, related_name='prices')
    value = models.DecimalField(_("Price"), decimal_places=2, max_digits=100, blank=True, null=True, default=None)

    def __unicode__(self):
        return "%s (%s)" % (self.value, self.product.name)

    class Meta:
        verbose_name = _("price")
        verbose_name_plural = _("prices")


class Order(models.Model):
    #!!order number
    #status
    customer = models.ForeignKey(Customer, null=False, blank=False, related_name="orders")
    date = models.DateField(_("Order date"), default=timezone.now)
    delivery_date = models.DateField(_("Delivery date"), default=timezone.now)
    notes = models.TextField(_("Notes"), blank=True)

    def __unicode__(self):
        return "Order #%s" % self.id

    class Meta:
        verbose_name = _("order")
        verbose_name_plural = _("orders")

    def save(self, *args, **kwargs):
        super(Order, self).save(*args, **kwargs)
        if not settings.ENABLE_DELIVERY_CENTER_DATE:
            self.details.update(delivery_date=self.delivery_date)


class OrderDetails(models.Model):
    order = models.ForeignKey(Order, related_name='details')
    delivery_center = models.ForeignKey(CustomerDistributionCenter, related_name='order_details')
    delivery_code = models.CharField(_("Delivery code"), max_length=256, null=False, blank=False)
    delivery_date = models.DateField(_("Delivery Date"), default=timezone.now)
    #substatus
    # Address - store copy
    country = models.CharField(_("Country"), max_length=100, null=False, blank=True)
    city = models.CharField(_("City"), max_length=100, null=False, blank=True)
    street = models.CharField(_("Street"), max_length=100, null=False, blank=True)
    street_number = models.CharField(_("Street Number"), max_length=10, null=False, blank=True)
    flat_number = models.CharField(_("Flat Number"), max_length=10, null=False, blank=True)
    zip = models.CharField(_("Postal code"), max_length=15, null=False, blank=True)
    address_notes = models.TextField("Address notes", null=False, blank=True)

    class Meta:
        verbose_name = _("order detail")
        verbose_name_plural = _("order detail")

    def __unicode__(self):
        return "#%s (%s)" % (self.delivery_code, self.delivery_date)

    def save(self, *args, **kwargs):
        fields = ['country', 'city', 'street', 'street_number',
                  'flat_number', 'zip', 'address_notes']
        for field in fields:
            if getattr(self, field) in [None, '']:
                setattr(self, field, getattr(self.delivery_center, field, ''))
        # Copy delivery date from oder to detials
        if self.order_id and not settings.ENABLE_DELIVERY_CENTER_DATE:
            self.delivery_date = self.order.delivery_date

        return super(OrderDetails, self).save(*args, **kwargs)


class OrderEntry(models.Model):
    order_details = models.ForeignKey(OrderDetails, related_name='entries')
    product = models.ForeignKey(Product, null=False, blank=False, related_name='order_entries')
    amount = models.PositiveIntegerField(_("Amount"), blank=False, null=False)
    if settings.ENABLE_PRICES:
        manual_price = models.DecimalField(_("Manual Price"), decimal_places=2, max_digits=100, blank=True, null=True, default=None)
        price = models.ForeignKey("Price", blank=True, null=True, default=None)

    class Meta:
        verbose_name = _("order entry")
        verbose_name_plural = _("order entry")

    def __unicode__(self):
        return "%s x %s" % (self.product, self.amount)

    @property
    def product_assumed_weight(self):
        return self.product.assumed_weight * self.amount

    @property
    def product_real_weight(self):
        return self.product.real_weight * self.amount

    @property
    def total_assumed_weight(self):
        return (self.product.assumed_weight + self.product.packaging.weight) * self.amount

    @property
    def total_real_weight(self):
        return (self.product.real_weight + self.product.packaging.weight) * self.amount

    @property
    def per_pallet(self):
        # containers on pallet property
        return self.amount / float(self.product.packaging.container.per_pallet)
