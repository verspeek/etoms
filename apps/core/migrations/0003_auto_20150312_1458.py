# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150311_1438'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerdistributioncenter',
            name='default_products',
            field=models.ManyToManyField(db_constraint='Default Products', null=True, to='core.Product', related_name='default_for'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='order',
            field=models.IntegerField(null=True, verbose_name='Product order', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='price',
            name='product',
            field=models.ForeignKey(related_name='prices', to='core.Product'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='priceperiod',
            name='date_to',
            field=models.DateField(default=None, null=True, verbose_name='To date', blank=True),
            preserve_default=True,
        ),
    ]
