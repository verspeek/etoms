__author__ = 'kosz85'


def create_registry_metaclass(key_field='codename', registry_field='registry'):
    """ Factory for creating registry metaclass,
    """
    class RegistryMetaClass(type):
        def __init__(cls, name, bases, nmspc):
            super(RegistryMetaClass, cls).__init__(name, bases, nmspc)
            if not hasattr(cls, registry_field):
                setattr(cls, registry_field, {})
            if getattr(cls, key_field, None) is not None:
                getattr(cls, registry_field)[getattr(cls, key_field)] = cls
    return RegistryMetaClass


def create_list_registry_metaclass(key_field='codename', registry_field='registry'):
    """
        Factory for creating ordered registry metaclass
    """
    class RegistryMetaClass(type):

        def __init__(cls, name, bases, nmspc):
            super(RegistryMetaClass, cls).__init__(name, bases, nmspc)
            if not hasattr(cls, registry_field):
                setattr(cls, registry_field, list())
            if getattr(cls, key_field, None) is not None:
                getattr(cls, registry_field).append(cls)

    return RegistryMetaClass
