import datetime
from collections import OrderedDict
from django.contrib import admin
from django import forms
from django.shortcuts import render_to_response
from django.conf.urls import patterns, url
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from grappelli_nested import admin as nested_admin
from daterange_filter.filter import DateRangeFilter
from django.contrib.admin import DateFieldListFilter
from reports import PickListReport, PickListDeliveryReport
admin.NestedModelAdmin = nested_admin.NestedModelAdmin
admin.NestedStackedInline = nested_admin.NestedStackedInline
admin.NestedTabularInline = nested_admin.NestedTabularInline

from .models import (
    Race, Package, Container, PackageAmount, TradeClass,
    Product, ProductRaceConfig,
    PlantStock, PackageStock, ContainerStock,
    CustomerDistributionCenter, Customer,
    Order, OrderEntry, OrderDetails, Price, PricePeriod)

from .forms import CustomDateRangeForm

admin.site.disable_action('delete_selected')


# ========= Base ==============
class PackageAmountInline(admin.TabularInline):
    model = PackageAmount
    extra = 1


@admin.register(Package)
class PackageAdmin(admin.ModelAdmin):
    list_display = ('name', 'weight', 'description')
    inlines = [PackageAmountInline]

    def description(self, obj):
        return obj.description if len(obj.description) < 100 else obj.description[:97] + '...'


@admin.register(Container)
class ContainerAdmin(admin.ModelAdmin):
    list_display = ('name', 'per_pallet', 'weight', 'description')
    inlines = [PackageAmountInline]

    def description(self, obj):
        return obj.description if len(obj.description) < 100 else obj.description[:97] + '...'


@admin.register(Race)
class RaceAdmin(admin.ModelAdmin):
    list_display = ('name', 'avg_weight', 'description')

    def description(self, obj):
        return obj.description if len(obj.description) < 100 else obj.description[:97] + '...'


@admin.register(TradeClass)
class TradeClassAdmin(admin.ModelAdmin):
    list_display = ('name',)


# ========= Stock =============

@admin.register(PlantStock)
class PlantStockAdmin(admin.ModelAdmin):
    list_display = ('race', 'amount',)


@admin.register(PackageStock)
class PackageStockAdmin(admin.ModelAdmin):
    list_display = ('package', 'amount',)


@admin.register(ContainerStock)
class ContainerStockAdmin(admin.ModelAdmin):
    list_display = ('container', 'amount',)


# ========= Product =============
class ProductRaceConfigFormset(forms.models.BaseInlineFormSet):
    def clean(self):
        # get forms that actually have valid data
        count = 0
        sum = 0
        for form in self.forms:
            if getattr(form, 'cleaned_data', False):
                count += 1
                sum += form.cleaned_data.get('percent', 0)
        if count < 1:
            raise forms.ValidationError(_("You must choose at least one race"))
        if sum > 100:
            raise forms.ValidationError(_("Sum of percents can't be bigger then 100%"))
        if sum < 99:
            raise forms.ValidationError(_("Sum of percents has to be at least 99%"))


class ProductRaceConfigInline(admin.TabularInline):
    model = ProductRaceConfig
    formset = ProductRaceConfigFormset
    classes = ('grp-collapse grp-open',)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'name', 'order', 'trade_class', 'assumed_weight', 'real_weight', 'in_stock')
    readonly_fields = ('in_stock',)
    inlines = [ProductRaceConfigInline]

    def in_stock(self, obj):
        list = [getattr(r.stock.order_by('-date').first(), 'amount', 0) for r in obj.races.all()]
        return list and min(list) or 0


# ========== Customer ================
class CustomerDistributionCenterInline(nested_admin.NestedStackedInline):
    template = 'admin/core/edit_inline/stacked.html'
    model = CustomerDistributionCenter
    extra = 0

    class Media:
        js = ('js/monkeypatch/grp_nested_inline.js',)

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        # Ordering inside default_products
        if db_field.name == 'default_products':
            kwargs['queryset'] = Product.objects.extra(
                select={'order_null': '"core_product"."order" is NULL'},
                order_by=['order_null', 'order'])
        return super(CustomerDistributionCenterInline, self).formfield_for_manytomany(db_field, **kwargs)


class AllProductsPriceFormset(forms.models.BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        # This all is for all products at priceperiod definition
        if kwargs.get('queryset'):
            kwargs['queryset'] = kwargs['queryset'].select_related('product').extra(
                select={'order_null': '"core_product"."order" is NULL'},
                order_by=['order_null', '"core_product"."order"'])
        super(AllProductsPriceFormset, self).__init__(*args, **kwargs)
        products = Product.objects.extra(
            select={'order_null': '"core_product"."order" is NULL'},
            order_by=['order_null', 'order'])
        if kwargs.get('instance'):
            products = products.exclude(prices__period=kwargs.get('instance'))
        self.initial_extra = [{'product': p.id} for p in products]


class PriceInline(nested_admin.NestedTabularInline):
    fields = ('product', 'value')
    model = Price
    formset = AllProductsPriceFormset
    template = 'admin/core/edit_inline/tabular.html'
    inline_classes = ('grp-collapse grp-open',)
    extra = 1

    class Media:
        js = ('js/monkeypatch/grp_nested_inline.js',)

    def get_extra(self, request, obj=None, **kwargs):
        # This all is for all products at priceperiod definition
        # Here we are counting how many are not created
        if obj is None:
            return Product.objects.count()
        return Product.objects.count() - obj.prices.count()


class PricePeriodInline(nested_admin.NestedStackedInline):
    fields = (('date_from', 'date_to'),)
    template = 'admin/core/edit_inline/stacked.html'
    model = PricePeriod
    extra = 0
    inlines = [PriceInline]

    class Media:
        js = ('js/monkeypatch/grp_nested_inline.js',)

@admin.register(Customer)
class CustomerAdmin(nested_admin.NestedModelAdmin):
    list_display = ('name', 'country', 'city')
    if settings.ENABLE_PRICES:
        inlines = [CustomerDistributionCenterInline, PricePeriodInline]
    else:
        inlines = [CustomerDistributionCenterInline]

    class Media:
        js = ('js/monkeypatch/grp_nested_inline.js',)
    # def delete_model(self, request, obj):
    #     if not obj.orders.exists():
    #         return super(CustomerAdmin, self).delete_model(self, request, obj)
    #     raise forms.ValidationError(_("Can't delete customer used in existing orders"))


@admin.register(CustomerDistributionCenter)
class CustomerDistributionCenterAdmin(admin.ModelAdmin):
    list_display = ('name', 'country', 'city', 'street', 'street_number', 'flat_number', 'zip')

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'default_products':
            kwargs['queryset'] = Product.objects.extra(
                select={'order_null': '"core_product"."order" is NULL'},
                order_by=['order_null', 'order'])
            kwargs['widget'] = forms.CheckboxSelectMultiple
        return super(CustomerDistributionCenterAdmin, self).formfield_for_manytomany(db_field, **kwargs)

    # def delete_model(self, request, obj):
    #     if not obj.order_details.exists():
    #         return super(CustomerDistributionCenterAdmin, self).delete_model(self, request, obj)
    #     raise forms.ValidationError(_("Can't delete distribution ceneter used in existing orders"))


# ========== Order ===============
if settings.ENABLE_PRICES:
    @admin.register(Price)
    class PriceAdmin(admin.ModelAdmin):
        list_display = ('product', 'value')


class DefaultProductsOrderEntryFormset(forms.models.BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        # This all is for all default products for CDC at OrderDetails inline
        if kwargs.get('queryset'):
            kwargs['queryset'] = kwargs['queryset'].select_related('product').extra(
                select={'order_null': '"core_product"."order" is NULL'},
                order_by=['order_null', '"core_product"."order"'])
        instance = kwargs.get('instance')
        if instance is not None and instance.delivery_center_id:
            products = Product.objects.filter(default_for=instance.delivery_center).exclude(
                order_entries=instance.entries.all()).extra(
                select={'order_null': '"core_product"."order" is NULL'},
                order_by=['order_null', 'order'])
            kwargs['initial'] = [{'product': p.id} for p in products]

        super(DefaultProductsOrderEntryFormset, self).__init__(*args, **kwargs)

    def clean(self):
        for form in self.forms:
            if 'amount' not in form.cleaned_data:
                if 'amount' in form.errors:
                    del form.errors['amount']
                form.do_not_save = True
                form.clean = lambda x=None: True
                form.is_valid = lambda x=None: True
                form.save = lambda x=None, commit=None: True
            else:
                if settings.ENABLE_PRICES and not form.cleaned_data.get('price') and not form.cleaned_data.get('manual_price'):
                    form.errors.update({'manual_price': [u'Enter a number.']})
                    form.errors.update({'price': [u'Select price.']})
                    raise forms.ValidationError(_('You must provide some price information.'))
        return super(DefaultProductsOrderEntryFormset, self).clean()

    def save_new(self, form, commit=True):
        if getattr(form, 'do_not_save', False):
            return form.Meta.model()
        return super(DefaultProductsOrderEntryFormset, self).save_new(form, commit=commit)

    @property
    def deleted_forms(self):
        ret = super(DefaultProductsOrderEntryFormset, self).deleted_forms
        for form in [form for form in self.forms if getattr(form, 'do_not_save', None) and getattr(form, 'instance')]:
            if form not in ret:
                ret.append(form)
        return ret


class OrderEntryInline(nested_admin.NestedTabularInline):
    template = 'admin/core/edit_inline/tabular.html'
    classes = ('grp-collapse grp-open',)
    inline_classes = ('grp-collapse grp-open',)
    formset = DefaultProductsOrderEntryFormset
    model = OrderEntry
    extra = 1
    raw_id_fields = ('product',)
    autocomplete_lookup_fields = {
        'fk': ['product'],
    }

    class Media:
        js = ('js/monkeypatch/grp_nested_inline.js',)

    def get_extra(self, request, obj=None, **kwargs):
        # This all is for all default products for CDC at OrderDetails inline
        # Here we are counting how many are not created
        if obj is None or obj.delivery_center_id is None:
            return 1
        return obj.delivery_center.default_products.count() - obj.entries.filter(
            product__in=obj.delivery_center.default_products.all()).count() + 1


class OrderDetailsFormset(forms.models.BaseInlineFormSet):
    def clean(self):
        for form in self.forms:
            order = form.cleaned_data.get('order')
            delivery_date = form.cleaned_data.get('delivery_date')
            if settings.ENABLE_DELIVERY_CENTER_DATE and order and order.date > delivery_date:
                raise forms.ValidationError(_("Delivery date should be same or later then order date"))


class OrderDetailsInline(nested_admin.NestedStackedInline):
    #template = 'admin/core/order/edit_inline/stacked.html'
    template = 'admin/core/edit_inline/stacked.html'
    classes = ('grp-collapse grp-open', 'etoms-order-details d-hide')
    inline_classes = ('grp-collapse grp-open',)
    inlines = [OrderEntryInline]
    formset = OrderDetailsFormset
    model = OrderDetails
    extra = 0
    if settings.ENABLE_DELIVERY_CENTER_DATE:
        fieldsets = (
            ('', {
                'fields': ('delivery_center', 'delivery_code', 'delivery_date'),
            }),
            (_('Advanced address options (click to open)'), {
                'classes': ('grp-collapse grp-closed',),
                'fields': ('country', 'city', 'street', 'street_number', 'flat_number', 'zip', 'address_notes')
            })
        )
    else:
        fieldsets = (
            ('', {
                'fields': ('delivery_center', 'delivery_code'),
            }),
            (_('Advanced address options (click to open)'), {
                'classes': ('grp-collapse grp-closed',),
                'fields': ('country', 'city', 'street', 'street_number', 'flat_number', 'zip', 'address_notes')
            })
        )
    #raw_id_fields = ('delivery_center',)
    #autocomplete_lookup_fields = {
    #    'fk': ['delivery_center'],
    #}

    class Media:
        js = ('js/monkeypatch/grp_nested_inline.js',)


class OrderCDCFilter(admin.SimpleListFilter):
    title = _('Distribution Center')
    parameter_name = 'cdc'

    def lookups(self, request, model_admin):
        return CustomerDistributionCenter.objects.values_list('id', 'name')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(details__delivery_center=self.value())
        return queryset


class OrderProductFilter(admin.SimpleListFilter):
    title = _('Product')
    parameter_name = 'product'

    def lookups(self, request, model_admin):
        return Product.objects.values_list('id', 'name')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(details__entries__product=self.value())
        return queryset


class OrderEntryProductFilter(admin.SimpleListFilter):
    title = _('Product')
    parameter_name = 'product'

    def lookups(self, request, model_admin):
        return Product.objects.values_list('id', 'name')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(product_id=self.value())
        return queryset


class OrderEntryCDCFilter(admin.SimpleListFilter):
    title = _('Distribution Center')
    parameter_name = 'cdc'

    def lookups(self, request, model_admin):
        return CustomerDistributionCenter.objects.values_list('id', 'name')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(order_details__delivery_center_id=self.value())
        return queryset


class AdminDateRangeFilter(DateRangeFilter):

    def get_form(self, request):
        return CustomDateRangeForm(
            request, data=self.used_parameters, field_name=self.field_path
        )


class EtomsDateFieldListFilter(DateFieldListFilter):
    def __init__(self, *args, **kwargs):
        super(EtomsDateFieldListFilter, self).__init__(*args, **kwargs)
        self.links = list(self.links)

        today = datetime.datetime.now().date()
        # At python 0 is monday, we want start from saturday
        weekday = (today.weekday() + 2) % 7
        self.links.insert(0, (_('This week'), {
            self.lookup_kwarg_since: str(today - datetime.timedelta(days=weekday)),
            self.lookup_kwarg_until: str(today + datetime.timedelta(days=7 - weekday)),
        }))


@admin.register(OrderEntry)
class OrderEntryAdmin(admin.ModelAdmin):
    list_display = ('delivery_code', 'product', 'amount', 'delivery_center', 'total_real_weight')
    list_filter = (
        ('order_details__delivery_date', EtomsDateFieldListFilter),
        OrderEntryProductFilter,
        OrderEntryCDCFilter,
        ('order_details__delivery_date', AdminDateRangeFilter),
    )
    actions = ['make_report']

    class Media:
        js = ('js/core/order_filter.js',)
        css = {
            'all': ('css/admin/order.css',)
        }

    @classmethod
    def delivery_code(cls, order_entry):
        return order_entry.order_details.delivery_code

    @classmethod
    def delivery_center(cls, order_entry):
        return order_entry.order_details.delivery_center.name

    def make_report(self, request, queryset):
        wanna_print = True
        order_entries = queryset.prefetch_related(
            'order_details', 'product'
        )
        order_entries_dict = OrderedDict()
        totals = dict()
        for entry in order_entries:
            if entry.product.name not in order_entries_dict:
                order_entries_dict[entry.product.name] = list()
            order_entries_dict[entry.product.name].append(entry)
        for key, values in order_entries_dict.items():
            for entry in values:
                if not key in totals:
                    totals[key] = {
                        'amount': 0,
                        'weight': 0
                    }
                totals[key]['amount'] = totals[key]['amount'] + entry.amount
                totals[key]['weight'] = totals[key]['weight'] + entry.total_real_weight
        date_from = request.GET.get('order_details__delivery_date__gte') or request.GET.get('order_details__delivery_date__gt')
        date_to = request.GET.get('order_details__delivery_date__lte') or request.GET.get('order_details__delivery_date__lt')
        cdc = product = None
        product_id = request.GET.get('product')
        cdc_id = request.GET.get('cdc')
        if cdc_id:
            try:
                cdc = CustomerDistributionCenter.objects.get(id=cdc_id)
            except (KeyError, ValueError, CustomerDistributionCenter.DoesNotExist):
                cdc = None
        if product_id:
            try:
                product = Product.objects.get(id=product_id)
            except (KeyError, ValueError, CustomerDistributionCenter.DoesNotExist):
                product = None
        return render_to_response('admin/core/product/report.html', {
            'base_template': 'admin/base_print.html' if wanna_print else 'admin/base_site.html',
            'noprint': not wanna_print,
            'order_entries_dict': order_entries_dict,
            'totals': totals,
            'product': product,
            'cdc': cdc,
            'date_from': date_from,
            'date_to': date_to
        })
    make_report.short_description = "Make Product Report"



@admin.register(Order)
class OrderAdmin(nested_admin.NestedModelAdmin):
    list_display = ('delivery_codes', 'get_date', 'get_delivery_date', 'customer', 'delivery_centers')
    list_filter = (
        ('date', EtomsDateFieldListFilter),
        ('delivery_date', EtomsDateFieldListFilter),
        'customer',
        OrderCDCFilter,
        OrderProductFilter,
    )
    inlines = [OrderDetailsInline]
    raw_id_fields = ('customer',)
    autocomplete_lookup_fields = {
        'fk': ['customer'],
    }
    actions = ['make_report']

    class Media:
        js = ('js/core/order.js',)
        css = {
            'all': ('css/admin/order.css',)
        }

    def get_date(self, obj):
        if obj.date > timezone.now().date():
            return '<span class="is-future-active-order">%s</span>' % obj.date
        elif obj.date == timezone.now().date():
            return '<span class="is-today-active-order">%s</span>' % obj.date
        else:
            return '<span>%s</span>' % obj.date
    get_date.allow_tags = True
    get_date.short_description = _('Order date')

    def get_delivery_date(self, obj):
        return '<span>%s</span>' % obj.delivery_date
    get_delivery_date.allow_tags = True
    get_delivery_date.short_description = _('Delivery date')

    def delivery_codes(self, obj):
        return ', '.join(od.delivery_code for od in obj.details.all())

    def delivery_centers(self, obj):
        return ', '.join(cdc.name for cdc in CustomerDistributionCenter.objects.filter(order_details__order=obj).only('name'))

    def make_report(self, request, queryset):
        wanna_print = True #'print' in request.GET
        orders = queryset.prefetch_related(
            'details', 'details__entries__product', 'customer')
        return render_to_response('admin/core/order/report.html', {
            'base_template': 'admin/base_print.html' if wanna_print else 'admin/base_site.html',
            'noprint': not wanna_print,
            'orders': orders,
        })
    make_report.short_description = "Make Order Report"

    def get_urls(self):
        urls = super(OrderAdmin, self).get_urls()
        my_urls = patterns('',
            url(r'^report/$', self.report, name='order-report'),
            url(r'^picklist/(?P<date>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})$', self.picklist, name='picklist-detail'),
            url(r'^picklist/$', self.picklist, name='picklist-list'),
            url(r'^picklist/(?P<date>[0-9]{4}-[0-9]{1,2}-[0-9]{1,2})/delivery/(?P<distribution_center>\d+)$',
                self.delivery_report, name='picklist-delivery-detail'),
        )
        return my_urls + urls

    def picklist(self, request, date=None):
        if date is None:
            return render_to_response('admin/core/order/picklist.html', {
                'base_template': 'admin/base_site.html',
                'dates': OrderDetails.objects.filter(entries__isnull=False)
                                      .values_list('delivery_date')
                                      .order_by('-delivery_date')
                                      .distinct(),
            })
        else:
            return PickListReport.get(date=date).render(request)

    def delivery_report(self, request, date, distribution_center):
        return PickListDeliveryReport.get(date=date,
                                          report_id=PickListDeliveryReport.get_report_id(
                                              cdc_id=distribution_center
                                          )).render(request)

    def report(self, request):
        wanna_print = 'print' in request.GET
        orders = Order.objects.all().prefetch_related(
            'details', 'details__entries__product', 'customer')

        return render_to_response('admin/core/order/report.html', {
            'base_template': 'admin/base_print.html' if wanna_print else 'admin/base_site.html',
            'noprint': not wanna_print,
            'orders': orders,
        })


class CustomUserAdmin(UserAdmin):

    def save_model(self, request, obj, form, change):
        if not change:
            obj.is_staff = True
        super(CustomUserAdmin, self).save_model(request, obj, form, change)
        from django.contrib.auth.models import Permission
        perms = Permission.objects.filter(
            content_type__name__in=['customer', 'customer distribution center', 'order', 'order details', 'order entry']
        ).filter(codename__startswith='change_').values_list('id', flat=True)
        if not change and not obj.is_superuser:
            obj.user_permissions.add(*perms)


admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), CustomUserAdmin)

