# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150312_1458'),
    ]

    operations = [
        migrations.AlterField(
            model_name='price',
            name='value',
            field=models.DecimalField(decimal_places=2, default=None, max_digits=100, blank=True, null=True, verbose_name='Price'),
            preserve_default=True,
        ),
    ]
