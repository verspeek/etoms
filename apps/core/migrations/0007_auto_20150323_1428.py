# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20150317_1552'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderentry',
            name='manual_price',
            field=models.DecimalField(decimal_places=2, default=None, max_digits=100, blank=True, null=True, verbose_name='Manual Price'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='priceperiod',
            name='customer',
            field=models.ForeignKey(related_name='prices', to='core.Customer'),
            preserve_default=True,
        ),
    ]
