# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone

def prepare_data(apps, schema_editor):
    Order = apps.get_model("core", "Order")
    Order.objects.all().update(delivery_date=models.F('date'))

class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20150323_1428'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='container',
            options={'verbose_name': 'container', 'verbose_name_plural': 'containers'},
        ),
        migrations.AlterModelOptions(
            name='containerstock',
            options={'verbose_name': 'container stock', 'verbose_name_plural': 'container stock'},
        ),
        migrations.AlterModelOptions(
            name='customer',
            options={'verbose_name': 'customer', 'verbose_name_plural': 'customers'},
        ),
        migrations.AlterModelOptions(
            name='customerdistributioncenter',
            options={'verbose_name': 'customer distribution center', 'verbose_name_plural': 'customer distribution center'},
        ),
        migrations.AlterModelOptions(
            name='order',
            options={'verbose_name': 'order', 'verbose_name_plural': 'orders'},
        ),
        migrations.AlterModelOptions(
            name='orderdetails',
            options={'verbose_name': 'order detail', 'verbose_name_plural': 'order detail'},
        ),
        migrations.AlterModelOptions(
            name='orderentry',
            options={'verbose_name': 'order entry', 'verbose_name_plural': 'order entry'},
        ),
        migrations.AlterModelOptions(
            name='package',
            options={'verbose_name': 'package', 'verbose_name_plural': 'packages'},
        ),
        migrations.AlterModelOptions(
            name='packageamount',
            options={'verbose_name': 'package amount', 'verbose_name_plural': 'package amount'},
        ),
        migrations.AlterModelOptions(
            name='packagestock',
            options={'verbose_name': 'package stock', 'verbose_name_plural': 'package stock'},
        ),
        migrations.AlterModelOptions(
            name='plantstock',
            options={'verbose_name': 'plant stock', 'verbose_name_plural': 'plant stock'},
        ),
        migrations.AlterModelOptions(
            name='price',
            options={'verbose_name': 'price', 'verbose_name_plural': 'prices'},
        ),
        migrations.AlterModelOptions(
            name='priceperiod',
            options={'verbose_name': 'price period', 'verbose_name_plural': 'price periods'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ('order', 'name'), 'verbose_name': 'product', 'verbose_name_plural': 'products'},
        ),
        migrations.AlterModelOptions(
            name='productraceconfig',
            options={'verbose_name': 'product race config', 'verbose_name_plural': 'product race config'},
        ),
        migrations.AlterModelOptions(
            name='race',
            options={'verbose_name': 'race', 'verbose_name_plural': 'races'},
        ),
        migrations.AlterModelOptions(
            name='tradeclass',
            options={'verbose_name': 'trade class', 'verbose_name_plural': 'trade class'},
        ),
        migrations.RemoveField(
            model_name='orderentry',
            name='manual_price',
        ),
        migrations.RemoveField(
            model_name='orderentry',
            name='price',
        ),
        migrations.AddField(
            model_name='order',
            name='delivery_date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='Delivery date'),
            preserve_default=True,
        ),
        migrations.RunPython(prepare_data),
    ]
