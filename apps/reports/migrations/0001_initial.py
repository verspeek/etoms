# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type', models.CharField(max_length=128, verbose_name='Report type')),
                ('report_id', models.CharField(max_length=128, verbose_name=b'Report id')),
                ('version', models.PositiveIntegerField(default=1, verbose_name='Version')),
                ('data_format_version', models.PositiveIntegerField(default=0, verbose_name='data format version')),
                ('created_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Created on')),
                ('text_data', models.TextField(default=b'{}', verbose_name='Data')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ReportSeen',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('seen_on', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Seen on')),
                ('report', models.ForeignKey(to='reports.Report')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='report',
            unique_together=set([('type', 'report_id', 'version')]),
        ),
    ]
