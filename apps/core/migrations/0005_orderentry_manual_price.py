# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150316_1909'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderentry',
            name='manual_price',
            field=models.DecimalField(decimal_places=2, default=None, max_digits=100, blank=True, null=True, verbose_name='Price'),
            preserve_default=True,
        ),
    ]
