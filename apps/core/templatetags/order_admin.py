from django import template
from django.contrib.admin.templatetags.admin_list import result_headers, result_hidden_fields, results


register = template.Library()
@register.inclusion_tag("admin/core/order/change_list_results.html")
def order_list(cl):
    """
    Displays the headers and data list together
    """
    headers = list(result_headers(cl))
    num_sorted_fields = 0
    for h in headers:
        if h['sortable'] and h['sorted']:
            num_sorted_fields += 1
    return {'cl': cl,
            'result_hidden_fields': list(result_hidden_fields(cl)),
            'result_headers': headers,
            'num_sorted_fields': num_sorted_fields,
            'results': list(results(cl))}


@register.simple_tag
def order_entry_total(totals, key, prop):
    return totals.get(key, dict()).get(prop, None)