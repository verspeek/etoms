from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from .rest import router


urlpatterns = patterns('',
    url(r'^password_reset/$', auth_views.password_reset, name='admin_password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^ajax/', include(router.urls)),
    url(r'^', include(admin.site.urls))
)
