from django.conf.urls import patterns
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import get_object_or_404, Http404
from django.contrib import admin
from models import Report, BaseReportGenerator


@admin.register(Report)
class ReportAdmin(admin.ModelAdmin):
    exclude = ('text_data',)
    list_display = ('type', 'date', 'report_id', 'version', 'created_on', 'show_action',
                    'print_action')
    list_filter = ('date', 'type',)

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def show_action(self, obj):
        return (u'<a href="show_report/%(id)s/">' + unicode(_("Show")) + u'</a>') % obj.__dict__
    show_action.allow_tags = True
    show_action.short_description = _("Show")

    def print_action(self, obj):
        return (u'<a href="print_report/%(id)s/">' + unicode(_("Print")) + u'</a>') % obj.__dict__
    print_action.allow_tags = True
    print_action.short_description = _("Print")

    def get_urls(self):
        urls = super(ReportAdmin, self).get_urls()
        my_urls = patterns('', (r'^(?P<mode>(show|print))_report/(?P<pk>\d+)/$', self.show_report))
        return my_urls + urls

    def show_report(self, request, mode, pk):
        report = get_object_or_404(Report, pk=pk)
        return report.generator.render(request, mode=mode)
