# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_orderentry_manual_price'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderentry',
            name='price',
            field=models.ForeignKey(default=None, blank=True, to='core.Price', null=True),
            preserve_default=True,
        ),
    ]
