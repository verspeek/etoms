# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0002_auto_20150615_0837'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='report',
            options={'verbose_name_plural': 'Reports'},
        ),
        migrations.AlterField(
            model_name='report',
            name='report_id',
            field=models.CharField(default=b'', max_length=128, verbose_name=b'Report additional info', blank=True),
            preserve_default=True,
        ),
    ]
