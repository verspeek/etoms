from django.db.models.loading import get_model


class DbModelPickle(object):
    """ This class forces object to be pickled as Model type and object id,
    then on unpickle it retrieve it directly from db.
    This class should be placed before models.Model in inheritance list,
    other way it won't work.
    """
    def __reduce__(self):
        reduced = super(DbModelPickle, self).__reduce__()
        if reduced[2].get('id', None) is not None:
            return model_unpickle, (reduced[1][0], reduced[2]['id'])
        return reduced


def model_unpickle(model_id, obj_id):
    if isinstance(model_id, tuple):
        model = get_model(*model_id)
    else:
        # Backwards compat - the model was cached directly in earlier versions.
        model = model_id
    return model.objects.get(id=obj_id)
model_unpickle.__safe_for_unpickle__ = True
