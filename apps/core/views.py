from django.shortcuts import render

# Create your views here.
from django.views.generic.list import ListView
from django.utils import timezone
from .models import OrderDetails


class PickListListView(ListView):
    def get_queryset(self):
         return OrderDetails.objects.filter(entries__isnull=False).values_list('delivery_date').order_by('-delivery_date').distinct()

    def get_template_names(self):
        return ['admin/core/order/picklist.html']

    def get_context_data(self, **kwargs):
        context = super(PickListListView, self).get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['base_template'] = 'admin/base_site.html'
        return context

