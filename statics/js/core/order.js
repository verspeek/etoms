(function($) {
    window.$ = $;

    var fill_select_func_factory = function(prefix, postfix, $form, $empty){
        return function(data){
            //fill selects with data
            var options = '<option value="" selected="selected">---------</option>';
            for (var i = 0; i < data.length; ++i){
                options += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }
            if ($empty)
                $empty.find('select[id^=' + prefix + '][id$=' + postfix + ']').html(options);
            var $el = $form.find('select[id^=' + prefix + '][id$=' + postfix + ']');
            var $manual_price = $form.find('.manual_price').find('input');
            var $amount = $form.find('.amount').find('input');
            var i = -1;
            while ($el.length > 0) {
                var val = $el.val();
                $el.html(options).val(val);
                if (data.length == 1 && !$el.val() && !$manual_price.val() && $amount.val()){
                    $el.val(data[0].id);
                } else if (data.length == 1 && !$el.val()) {
                    $el.val(undefined);
                }
                $el.trigger('change');
                i += 1;
                $el = $form.find('select[id^=' + prefix + '][id$=' + i + postfix + ']');
            }
        }
    };

    var select_disable_all = function(prefix, postfix, $form){
        $form.find('select[name^=' + prefix + '][name$=' +  postfix + ']').html('<option value="" selected="selected">---------</option>');
    };

    var customer_selected = function(customer_id){
        $.getJSON('/ajax/customerdistributioncenter/', {customer: customer_id}, function(data){
            //fill selects with data
            var options = '<option value="" selected="selected">---------</option>';
            for (var i=0; i < data.length; ++i){
                options += '<option value="' + data[i].id + '">' + data[i].name + '</option>';
            }
            $('select#id_details-__prefix__-delivery_center').html(options);
            var i = 0;
            var $el = $('select#id_details-' + i + '-delivery_center');
            while ($el.length > 0){
                var val = $el.val();
                $el.html(options).val(val)
                if (data.length == 1 && !$el.val()){
                    $el.val(data[0].id);
                }
                $el.trigger('change');
                i += 1;
                $el = $('select#id_details-' + i + '-delivery_center');
            }
        });
    };

    $(document).ready(function() {

        $('#id_date').on('change', function() {
            var val = $(this).val();
            $('[name^=details-][name$=-delivery_date]').each(function () {
                if ($(this).val() < val) {
                    $(this).val(val);
                }
            });
        });


        $('.etoms-order-details .price select').on('change', function(event) {
            if (!event.originalEvent) {
                return;
            }
            var _self = $(this);
            if (_self.val()) {
                $(_self).closest('.grp-tr').find('.manual_price input').val(undefined);
                /*
                $.getJSON('/ajax/price/' + _self.val() + '/', {}, function(data) {
                    $(_self).parent().parent().find('.manual_price input').val(parseInt(data.value));
                });
                */
            }
        });

        $('.etoms-order-details .manual_price input').on('change', function() {
            var _self = $(this);
            if (_self.val()) {
                $(_self).closest('.grp-tr').find('.price select').val(undefined);
            }
        });

        $('#id_date').on('change', function() {
            var val = $(this).val();
            $('[name^=details-][name$=-delivery_date]').each(function () {
                if ($(this).val() < val) {
                    $(this).val(val);
                }
            });
        });

        $('.grp-add-handler').on('click', function() {
            var $this = $(this);
            setTimeout(function(){
                $this.parents('.grp-group').first().find('select').each(function(){
                    $(this).trigger('change');
                });
                $this.parents('.grp-group').first().find('input').each(function(){
                    $(this).trigger('change');
                });
            })
        });

        //Customer change
        $('input#id_customer').on('focus', function(){
            $(this).trigger('change');
        }).on('change', function(){
            var customer_id = $('input#id_customer').val();
            if (customer_id){
                customer_selected(customer_id);
                if ($('.etoms-order-details > .grp-items > .grp-dynamic-form').length == 0){
                    $('.etoms-order-details .grp-add-handler.details-group').first().click();
                }
                $('.etoms-order-details').removeClass('d-hide');
            } else {
                $('.etoms-order-details').addClass('d-hide');
            }
        }).trigger('change');

        //Delivery Center change (fill address placeholders and default products)
        $('body').on('focus', '[id^=id_details-][id$=-delivery_center]', function(){
            $(this).trigger('change');
        }).on('change', '[id^=id_details-][id$=-delivery_center]', function(event){
            var $this = $(this);
            if ($this.val() == $this.data('old-val')) return;
            var old_val = $this.data('old-val');
            $this.data('old-val', $this.val());
            if (!$this.val()) return;
            var list = ['country', 'city', 'street', 'street_number', 'flat_number', 'zip', 'address_notes'];
            var $form = $this.parents('.grp-dynamic-form').first();

            // first run is for preserving products on validation error after reload
            var first_run = !$(this).data('first-run');
            $(this).data('first-run', true);

            $.getJSON('/ajax/customerdistributioncenter/' + $this.val() +'/', {}, function(data) {
                //address
                for (var i = 0; i < list.length; ++i){
                    $form.find('[id^=id_details-][id$=-' + list[i] + ']').attr('placeholder', data[list[i]]);
                }

                var white_list = [];
                //delete products (saved - construct white_list)
                $form.find('.' + $form[0].id + '-entries-nested-inline .grp-delete-handler').each(function(){
                    if ($(this).parents('.grp-empty-form').length == 0){
                        var val = parseInt($(this).parents('.grp-tr').first().find('input[name$=-product]').val());
                        var deleted = $(this).parents('.grp-predelete').length != 0;
                        var to_be_deleted = data.default_products.indexOf(val) == -1 && !!old_val;
                        if((to_be_deleted && !deleted) || (!to_be_deleted && deleted))
                            $(this).click();
                        if (!isNaN(val))
                            white_list.push(val);
                    }
                });

                // first time preserve products with values, delete previous one (not saved)
                $form.find('.' + $form[0].id + '-entries-nested-inline .grp-remove-handler').each(function(){
                    if ($(this).parents('.grp-empty-form').length == 0){
                        var val = parseInt($(this).parents('.grp-tr').first().find('input[name$=-product]').val());
                        if (!isNaN(val) && first_run)
                            white_list.push(val);
                        else
                            $(this).click(); // remove
                    }
                });

                var ajax_active_was_zero = false;
                //set additional default products
                for (var i = 0; i < data.default_products.length; ++i){
                    if (white_list.indexOf(data.default_products[i]) != -1) continue;
                    $form.find('.' + $form[0].id  +"-entries-group.grp-add-handler").first().click();
                    var free = [];
                    $form.find('[name^=' + $form[0].id +"-][name$=-product]").each(function(){
                        if (this.id.split('__prefix__').length > 1) return;
                        if (!this.value)
                            free.push('#' + this.id);
                    });
                    //$(free.pop()).val(data.default_products[i]).trigger('change');
                    var input = free.pop();
                    var value = data.default_products[i];
                    // Firefox bug, there is value but it's not refreshing autocomplete
                    $(input)[0].value = value;
                    setTimeout(function(input, val){
                        var func = function(){
                            if ($.active == 0)
                                ajax_active_was_zero = true;
                            if (ajax_active_was_zero)
                                $(input).change();
                            else
                                setTimeout(func, 100);
                        };
                        return func;
                    }(input, data.default_products[i]), 100);
                }
            });
        });

        $('.nested-inline').on('click', function() {
            $('.nested-inline').find('.grp-td.amount').find('input').attr('autocomplete', 'off');
        });

        //Product change  - fetch price
        $('body').on('focus', '[id^=id_details-][id$=-product]', function(){
            $(this).trigger('change');
        }).on('change', '[id^=id_details-][id$=-product]', function(){
            var has_price = $(document).find('.price');
            if (has_price.length) {
                var $this = $(this);
                if ($this.attr('id').split('__prefix__').length > 1) return;
                var $form = $this.parents('.grp-dynamic-form').first();
                if (!$this.val()) {
                    //FIX THIS
                    select_disable_all('details-', '-price', $form);
                    return;
                }
                var $empty = $form.next('.grp-empty-form');
                $.getJSON('/ajax/price/', {
                    product: $this.val(),
                    period__customer: $('input#id_customer').val(),
                    period__date_from__gte: $('input#id_date').val(),
                    period__date_to__lte: $('input#id_date').val()
                }, fill_select_func_factory('id_details-', '-price', $form, $empty));
            }
        });

        $(document).on('keydown', function(event) {
            if (event.keyCode == 9) {
                event.preventDefault();
                var focusables = $('input,select,textarea');
                var active = document.activeElement;
                var current_index = undefined;
                var next = undefined;
                for (var i = 0; i < focusables.length; i++) {
                    var input = focusables[i];
                    if (input == active) {
                        current_index = i;
                    }
                }
                if (!current_index) {
                    current_index = 0;
                }
                for (var j = current_index + 1; j < focusables.length; j++) {
                    var next_input = focusables[j];
                    if ($(next_input).attr('readonly') == undefined && $(next_input).attr('type') != 'hidden') {
                        next = next_input;
                        break;
                    }
                }
                if (next == undefined) {
                    next = focusables[0];
                }
                $(next).focus();
            }
        });

        // Add classes in order to paint orders table rows
        var tr = $('tr');
        tr.has('.is-today-active-order').addClass('is-today-active-order-tr');
        tr.has('.is-future-active-order').addClass('is-future-active-order-tr');

    }); //ready
})(grp.jQuery);



