
import json
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.auth.models import User
from django.shortcuts import render_to_response
from django.dispatch import receiver
from django.core.signals import request_finished
from lib.utils import create_registry_metaclass


class BaseReportGenerator(object):
    __metaclass__ = create_registry_metaclass('type', 'reports')
    JOBS = set()
    type = None
    data_format_version = 0
    template = None

    def __init__(self, report=None):
        self.report = report

    @classmethod
    def add_job(cls, date, report_id='', type=None):
        if type is None:
            type = cls.type
        cls.JOBS.add((type, date, report_id))

    @classmethod
    def process_jobs(cls):
        while cls.JOBS:
            type, date, report_id = cls.JOBS.pop()
            cls.reports.get(type).generate(date, report_id)

    @classmethod
    def get_report_id(cls, **kwargs):
        return ''

    @classmethod
    def get(cls, date, report_id=''):
        return cls(report=cls.generate(date, report_id, check_new=False))

    @classmethod
    def generate(cls, date=None, report_id=None, check_new=True):
        if date is None:
            date = timezone.now().date()
        if check_new:
            data = cls.get_data(date, report_id)
        report = Report.objects.filter(
            type=cls.type, date=date, report_id=report_id).order_by('-version').first()
        if report is None or (check_new and data != report.data):
            report = Report(type=cls.type,
                            date=date,
                            report_id=report_id,
                            version=report and report.version + 1 or 1,
                            data_format_version=cls.data_format_version,
                            data=check_new and data or cls.get_data(date, report_id))
            report.save()
        return report

    @classmethod
    def get_data(cls, date, report_id=''):
        raise NotImplementedError('You need to implement this method')

    def migrate(self):
        raise NotImplementedError('You need to implement this method')

    def get_context(self):
        raise NotImplementedError('You need to implement this method')

    def get_previous_seen(self, user=None):
        repseen = ReportSeen.objects.filter(
            report__type=self.report.type,
            report__date=self.report.date,
            report__version__lt=self.report.version,
            report__report_id=self.report.report_id).exclude(
            report=self.report)
        if user:
            repseen = repseen.filter(user=user)
        repseen = repseen.select_related('report').order_by('-report__version').first()
        return repseen and repseen.report or None

    def render(self, request, mode=None):
        if self.template is None:
            raise NotImplementedError("You need to define template")
        if mode == 'print' or 'print' in request.GET:
            base = 'admin/reports/base_print.html'
            noprint = False
        else:
            base = 'admin/base_site.html'
            noprint = True
        context = {
            'base_template': base,
            'noprint': noprint,
        }
        context.update(self.get_context(request))
        if request.user.is_authenticated():
            ReportSeen.objects.get_or_create(report=self.report, user=request.user)
        return render_to_response(self.template, context)


class Report(models.Model):
    type = models.CharField(_("Report type"), max_length=128, null=False, blank=False)
    date = models.DateField(_("Date"), default=timezone.now)
    report_id = models.CharField("Report additional info", max_length=128, blank=True, default='')
    version = models.PositiveIntegerField(_("Version"), default=1)
    data_format_version = models.PositiveIntegerField(_("data format version"), default=0)
    created_on = models.DateTimeField(_("Created on"), default=timezone.now)
    text_data = models.TextField(_("Data"), default='{}')
    __data = None

    class Meta:
        unique_together = ('type', 'date', 'report_id', 'version')
        verbose_name_plural = _("Reports")

    def __unicode__(self):
        return "%s %s %s (V%s)" % (self.type, self.date, self.report_id, self.version)

    @property
    def generator(self):
        return BaseReportGenerator.reports.get(self.type)(report=self)

    @property
    def data(self):
        if self.__data is None:
            self.__data = json.loads(self.text_data)
        return self.__data

    @data.setter
    def data(self, value):
        self.text_data = json.dumps(value)
        self.__data = value


class ReportSeen(models.Model):
    report = models.ForeignKey(Report, null=False)
    user = models.ForeignKey(User, null=False)
    seen_on = models.DateTimeField(_("Seen on"), default=timezone.now)


@receiver(request_finished)
def reports_processing(sender, **kwargs):
    BaseReportGenerator.process_jobs()
