# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


def price_migration(apps, schema_editor):
    Price = apps.get_model("core", "Price")
    PricePeriod = apps.get_model("core", "PricePeriod")
    for p in Price.objects.filter(customer__isnull=False):
        to = p.date_to
        if to == django.utils.timezone.datetime(2222, 12, 22):
            to = None
        pp, created = PricePeriod.objects.get_or_create(
            customer=p.customer, date_from=p.date_from, date_to=to)
        p.period = pp
        p.save()
    Price.objects.exclude(customer__isnull=False).delete()


def price_back_migration(apps, schema_editor):
    Price = apps.get_model("core", "Price")
    for p in Price.objects.all():
        p.customer = p.period.customer
        p.date_from = p.period.date_from
        p.date_to = p.period.date_to or django.utils.timezone.datetime(2222, 12, 22)
        p.save()


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PricePeriod',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_from', models.DateField(default=django.utils.timezone.now, verbose_name='From date')),
                ('date_to', models.DateField(default=None, null=True, verbose_name='To date')),
                ('customer', models.ForeignKey(to='core.Customer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='price',
            name='period',
            field=models.ForeignKey(related_name='prices', to='core.PricePeriod', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='order',
            name='customer',
            field=models.ForeignKey(related_name='orders', to='core.Customer'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='price',
            name='customer',
            field=models.ForeignKey(to='core.Customer', null=True),
            preserve_default=True,
        ),
        migrations.RunPython(price_migration, price_back_migration),
        migrations.RemoveField(
            model_name='price',
            name='customer',
        ),
        migrations.RemoveField(
            model_name='price',
            name='date_from',
        ),
        migrations.RemoveField(
            model_name='price',
            name='date_to',
        ),
    ]
