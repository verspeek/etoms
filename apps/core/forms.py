from django import forms
from django.contrib.admin.widgets import AdminDateWidget
from django.utils.translation import ugettext as _
from daterange_filter.filter import DateRangeFilterBaseForm


class CustomDateRangeForm(DateRangeFilterBaseForm):

    def __init__(self, *args, **kwargs):
        field_name = kwargs.pop('field_name')
        super(CustomDateRangeForm, self).__init__(*args, **kwargs)

        self.fields['%s__gte' % field_name] = forms.DateField(
            label='',
            widget=AdminDateWidget(
                attrs={'placeholder': _('From date'), 'type': 'date', 'class': 'vDateField filter-date-range'}
            ),
            localize=True,
            required=False
        )

        self.fields['%s__lte' % field_name] = forms.DateField(
            label='',
            widget=AdminDateWidget(
                attrs={'placeholder': _('To date'), 'type': 'date', 'class': 'vDateField filter-date-range'}
            ),
            localize=True,
            required=False,
        )