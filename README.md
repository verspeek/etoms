# E-Toms

### Setuping project

    ::bash
    # checkout repo and open it's directory
    
    sudo apt-get install python2.7 python2.7-dev virtualenv virtualenvwrapper pip
    sudo pip install --upgrade pip
    sudo pip install --upgrade virtualenv
    # there maybe need to configure bash with virtualenvwrapper
    # but you can just create virtualenv
    mkvirtualenv etoms
    # activate virtualenv
    workon etoms
    pip install -r requirements.txt
    ./manage.py migrate
    ./manage.py createsuperuser
    
 
 Install postgresql version doesn't matter it runes even on sqlLite 

    ::bash
    su postgres
    psql
    CREATE USER etoms with PASSWORD 'etoms';
    CREATE DATABASE etoms;
    GRANT ALL PRIVILEGES ON DATABASE etoms TO etoms;
    
    
### Running 
    ./manage.py runserver 7000
    
### Upgrading servers
    . etoms.sh
    git pull
    pip install -r requirements.txt  # if needed
    ./manage.py migrate
    ./manage.py collectstatic
    touch etoms/wsgi.py
