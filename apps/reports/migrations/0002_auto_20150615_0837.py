# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='date',
            field=models.DateField(default=django.utils.timezone.now, verbose_name='Date'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='report',
            name='report_id',
            field=models.CharField(default=None, max_length=128, null=True, verbose_name=b'Report id'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='report',
            unique_together=set([('type', 'date', 'report_id', 'version')]),
        ),
    ]
