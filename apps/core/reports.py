# -*- coding: utf-8 -*-
from django.db.models import Sum, Q, Aggregate
from django.db.models.sql.aggregates import Avg as SQLAggregate
from django.utils.translation import ugettext_lazy as _
from apps.reports.models import BaseReportGenerator
from models import CustomerDistributionCenter, Product, Order, OrderEntry, OrderDetails
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver


class PickListReport(BaseReportGenerator):
    type = 'picklist'
    template = 'admin/core/order/picklist.html'

    @classmethod
    def get_data(cls, date, report_id=''):
        """
        :param date: date string
        :param report_id: not used pass None
        :return: data
        """
        sum_col = []
        total_amount = 0
        total_pallets = 0
        data = []

        cdc = CustomerDistributionCenter.objects.filter(order_details__delivery_date=date).distinct().order_by('name').values('id', 'name')
        products = Product.objects.filter(Q(order_entries__order_details__delivery_date=date) | Q(order__isnull=False))
        products = products.distinct().extra(
            select={'order_null': '"core_product"."order" is NULL'},
            order_by=['order_null', 'order'])
        order_null = False
        for product in products:
            if not order_null and product.order_null:
                order_null = True
            else:
                product.order_null = False

            row = {
                'product': {
                    'id': product.id,
                    'name': product.name,
                    'order_null': product.order_null,
                },
                'items': []
            }

            sum_amount = 0
            sum_pallets = 0
            i = -1
            for dc in cdc:
                i += 1
                tmp = CustomerDistributionCenter.objects.filter(
                    id=dc['id'],
                    order_details__delivery_date=date,
                    order_details__entries__product=product
                ).annotate(amount=Sum('order_details__entries__amount')).first()
                if len(sum_col) == i:
                    sum_col.append({'amount': 0,
                                    'pallets': 0})
                if tmp:
                    if float(product.packaging.container.per_pallet):
                        pallets = tmp.amount / float(product.packaging.container.per_pallet)
                    else:
                        pallets = 0
                    sum_amount += tmp.amount
                    sum_pallets += pallets
                    total_amount += tmp.amount
                    total_pallets += pallets
                    sum_col[i]['amount'] += tmp.amount
                    sum_col[i]['pallets'] += pallets
                    row['items'].append({
                        'amount': tmp.amount,
                        'pallets': pallets
                    })
                else:
                    row['items'].append({})

            row['items'].append({
                'amount': sum_amount,
                'pallets': sum_pallets,
            })
            data.append(row)

        return {
            'cdc': list(cdc),
            'data': data,
            'sum_col': sum_col,
            'total_amount': total_amount,
            'total_pallets': total_pallets,
        }

    def prepare_data(self, data):
        # First row
        header = [{'name': _('Product')}] + data['cdc'] + [{'name': u'Σ'}]
        # Body
        table = data['data']
        # Last row
        last_row = data['sum_col']
        last_row.append({
            'amount': data['total_amount'],
            'pallets': data['total_pallets'],
        })
        table.append({
            'product': {
                'id': None,
                'name': u'Σ',
                'order_null': None,
            },
            'items': last_row
        })
        return {
            'header': header,
            'table': data['data'],
        }

    def get_context(self, request, data=None):
        if data is None:
            data = self.report.data

        data = self.prepare_data(data)
        last = self.get_previous_seen(request.user)
        if last:
            last_data = self.prepare_data(last.data)
            for h in data['header']:
                if h not in last_data['header']:
                    h['color'] = True
            i = 0
            products = {}
            for t in last_data['table']:
                if t['product']['id']:
                    products[t['product']['id']] = i
                i += 1

            for t in data['table']:
                if t not in last_data['table'] and t['product']['id']:
                    index = products.get(t['product']['id'])
                    if index is None:
                        t['product']['color'] = True
                        for i in t['items']:
                            i['color'] = True
                    else:
                        last_items = last_data['table'][index]['items']
                        for i in t['items']:
                            if i not in last_items:
                                i['color'] = True
        return {
            'current_date': self.report.date,
            'current_version': self.report.version,
            'current_version_datetime': self.report.created_on,
            'last_seen_version': last and last.version,
            'last_seen_version_datetime': last and last.created_on,
            'header': data['header'],
            'table': data['table'],
        }

@receiver(pre_save, sender=Order)
def order_pre_save(sender, instance, **kwargs):
    if instance.id:
        PickListReport.add_job(instance.delivery_date)

@receiver(pre_save, sender=OrderDetails)
def order_details_pre_save(sender, instance, **kwargs):
    if instance.id:
        PickListReport.add_job(instance.delivery_date)

@receiver(post_save, sender=Order)
def order_post_save(sender, instance, **kwargs):
    PickListReport.add_job(instance.delivery_date)

@receiver(post_save, sender=OrderEntry)
def order_entry_post_save(sender, instance, **kwargs):
    PickListReport.add_job(instance.order_details.delivery_date)

@receiver(post_save, sender=OrderDetails)
def order_details_post_save(sender, instance, **kwargs):
    PickListReport.add_job(instance.delivery_date)


class PickListDeliveryReport(BaseReportGenerator):
    type = 'picklist_delivery'
    template = 'admin/core/order/picklist_delivery_report.html'

    @classmethod
    def get_report_id(cls, cdc=None, cdc_id=None):
        return '%s' % (cdc_id or cdc.id)

    @classmethod
    def get_data(cls, date, report_id=''):
        cdc_id = report_id

        class SQLPallets(SQLAggregate):
            sql_function = 'sum'

            @property
            def sql_template(self):
                return 'sum(CAST("core_orderentry"."amount" AS float) / CAST("core_container"."per_pallet" AS float))'

        class Pallets(Aggregate):
            """New aggragate to count pallets"""
            def add_to_query(self, query, alias, col, source, is_summary):
                aggregate = SQLPallets(col, source=source, is_summary=is_summary, **self.extra)
                query.aggregates[alias] = aggregate

        # Retrive CDCs - CustomerDistributionCenters
        cdc = CustomerDistributionCenter.objects.only('id', 'name').get(id=cdc_id)
        # Retrive all used products and default products (with ordering)
        products = Product.objects.filter(
            Q(order_entries__order_details__delivery_date=date,
              order_entries__order_details__delivery_center=cdc) | Q(order__isnull=False)).extra(
            select={'order_null': '"core_product"."order" is NULL'},
            order_by=['order_null', 'order']).distinct()

        # Find & Mark first not default product (order == None)
        order_null = False
        for product in products:
            if not order_null and product.order_null:
                order_null = True
            else:
                product.order_null = False

        # Fetch aggragates
        order_details = OrderDetails.objects.distinct().filter(
            delivery_date=date, entries__isnull=False, delivery_center=cdc).distinct().prefetch_related(
            'entries', 'entries__product', 'entries__product__packaging__container').annotate(
            amount=Sum('entries__amount'),
            pallets=Pallets('entries__product__packaging__container__per_pallet')
        )

        # Construct data
        data = []
        for od in order_details:
            odi = {
                'entries': [],
                'id': od.id,
                'order_id': od.order_id,
                'delivery_code': od.delivery_code,
                'delivery_date': str(od.delivery_date),
                'delivery_center_name': od.delivery_center.name,
                'country': od.country,
                'city': od.city,
                'zip': od.zip,
                'street': od.street,
                'street_number': od.street_number,
                'flat_number': od.flat_number,
                'address_notes': od.address_notes,
                'pallets': od.pallets,
                'amount': od.amount,
            }
            entry_map = {}
            for entry in od.entries.all():
                entry_map[entry.product.id] = entry
            for product in products:
                entry = entry_map.get(product.id)
                item = {
                    'product': {
                        'id': product.id,
                        'name': product.name,
                        'order_null': product.order_null,
                    },
                }
                if entry:
                    item.update({
                        'id': entry.id,
                        'amount': entry.amount,
                        'per_pallet': entry.per_pallet,
                    })
                odi['entries'].append(item)
            data.append(odi)

        return {
            'cdc': {'name': cdc.name},
            'data': data,
        }

    def get_context(self, request, data=None):
        if data is None:
            data = self.report.data
        last = self.get_previous_seen(request.user)
        if last:
            last_data = last.data

            # Prepare map of old order_details
            last_odi_map = {}
            i = 0
            for odi in last_data['data']:
                last_odi_map[odi['id']] = i
                i += 1

            # Iterate and check for differences
            for odi in data['data']:
                if odi in last_data['data']:
                    continue
                odi_ind = last_odi_map.get(odi['id'])
                if odi_ind is None:
                    odi['color'] = True
                    for e in odi['entries']:
                        e['color'] = True
                    #todo: color entries
                    continue
                # check if odi pure data differs
                tmp_odi = odi.copy()
                del tmp_odi['entries']
                tmp_last_odi = last_data['data'][odi_ind].copy()
                last_entries = tmp_last_odi['entries']
                del tmp_last_odi['entries']
                if tmp_odi != tmp_last_odi:
                    if tmp_odi['delivery_code'] != tmp_last_odi['delivery_code']:
                        odi['color_delivery_code'] = True
                    elif tmp_odi['delivery_date'] != tmp_last_odi['delivery_date']:
                        odi['color_delivery_date'] = True
                    else:
                        odi['color_address'] = True

                # check entries
                for e in odi['entries']:
                    if e in last_entries:
                        continue
                    e['color'] = True

        return {
            'current_date': self.report.date,
            'current_version': self.report.version,
            'current_version_datetime': self.report.created_on,
            'last_seen_version': last and last.version,
            'last_seen_version_datetime': last and last.created_on,
            'data': data['data'],
            'cdc': data['cdc'],
        }

@receiver(post_save, sender=Order)
def delivery_order_post_save(sender, instance, **kwargs):
    for od in instance.details.all():
        PickListDeliveryReport.add_job(instance.delivery_date,
                                       str(od.delivery_center_id))

@receiver(post_save, sender=OrderEntry)
def delivery_order_entry_post_save(sender, instance, **kwargs):
    PickListDeliveryReport.add_job(instance.order_details.delivery_date,
                                   str(instance.order_details.delivery_center_id))

@receiver(post_save, sender=OrderDetails)
def delivery_order_details_post_save(sender, instance, **kwargs):
    PickListDeliveryReport.add_job(instance.delivery_date, str(instance.delivery_center_id))
