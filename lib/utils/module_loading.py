from django.conf import settings


def load_files(filename):
    for module in settings.INSTALLED_APPS:
        mod = '.'.join([module, filename])
        try:
            __import__(mod)
        except ImportError, e:
            if not e.args[0].startswith("No module named "):
                raise
