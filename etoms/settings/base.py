"""
Django settings for etoms project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'e=%@k9m&fnehtp4d#^@oz51nvp%%!+el(0!g7fkpgci5)ei(uh'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'grappelli.dashboard',
    'grappelli',
    'grappelli_nested',
    'rest_framework',
    'daterange_filter',
    #'filtrate',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'apps.core',
    'apps.reports',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.request',
    'django.core.context_processors.i18n',
    'django.contrib.messages.context_processors.messages',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

ROOT_URLCONF = 'etoms.urls'

WSGI_APPLICATION = 'etoms.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.postgresql_psycopg2',
#         'NAME': 'etoms',
#         'USER': 'etoms',
#         'PASSWORD': 'etoms',
#         'HOST': 'localhost',
#         'PORT': '',
#     }
# }

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

LOCALE_PATHS = (os.path.join(BASE_DIR, 'locale'), )

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/
GIT_REF = 'ref: HEAD'
try:
    while GIT_REF.startswith('ref: '):
        GIT_REF_FILE = os.path.join(BASE_DIR, ".git", GIT_REF.split(' ', 1)[-1])
        GIT_REF = open(GIT_REF_FILE).read().strip()
except IOError:
    import random
    GIT_REF = str(random.randint(0, 100000))
GIT_REF = 'ref-' + GIT_REF[:10] + '/'

STATIC_URL = os.path.join('/static/', GIT_REF)

STATIC_ROOT = os.path.join(BASE_DIR, 'htdocs/statics')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'statics'),
)

# ADMIN
GRAPPELLI_ADMIN_TITLE = 'E-Toms'
GRAPPELLI_INDEX_DASHBOARD = {
    'django.contrib.admin.site': 'etoms.dashboard.CustomIndexDashboard',
}

ENABLE_PRICES = False
ENABLE_DELIVERY_CENTER_DATE = False
