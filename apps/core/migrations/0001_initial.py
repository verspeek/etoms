# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Container',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('weight', models.DecimalField(verbose_name='Weight', max_digits=22, decimal_places=2)),
                ('per_pallet', models.PositiveIntegerField(verbose_name='Per pallet')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContainerStock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(default=django.utils.timezone.now, verbose_name='Date')),
                ('amount', models.PositiveIntegerField(null=True, verbose_name='Amount', blank=True)),
                ('container', models.ForeignKey(related_name='stock', to='core.Container')),
            ],
            options={
                'verbose_name_plural': 'Container Stock',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('contact_person', models.CharField(max_length=250, verbose_name='Contact Person', blank=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='Active')),
                ('notes', models.TextField(verbose_name='Notes', blank=True)),
                ('country', models.CharField(max_length=100, verbose_name='Country')),
                ('city', models.CharField(max_length=100, verbose_name='City')),
                ('street', models.CharField(max_length=100, verbose_name='Street', blank=True)),
                ('street_number', models.CharField(max_length=10, verbose_name='Street Number', blank=True)),
                ('flat_number', models.CharField(max_length=10, verbose_name='Flat Number', blank=True)),
                ('zip', models.CharField(max_length=15, verbose_name='Postal code', blank=True)),
                ('address_notes', models.TextField(verbose_name=b'Address notes')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CustomerDistributionCenter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('is_active', models.BooleanField(default=True, verbose_name='Active')),
                ('notes', models.TextField(verbose_name='Notes', blank=True)),
                ('country', models.CharField(max_length=100, verbose_name='Country')),
                ('city', models.CharField(max_length=100, verbose_name='City')),
                ('street', models.CharField(max_length=100, verbose_name='Street', blank=True)),
                ('street_number', models.CharField(max_length=10, verbose_name='Street Number', blank=True)),
                ('flat_number', models.CharField(max_length=10, verbose_name='Flat Number', blank=True)),
                ('zip', models.CharField(max_length=15, verbose_name='Postal code', blank=True)),
                ('address_notes', models.TextField(verbose_name=b'Address notes')),
                ('customer', models.ForeignKey(to='core.Customer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(default=django.utils.timezone.now, verbose_name='Date')),
                ('notes', models.TextField(verbose_name='Notes', blank=True)),
                ('customer', models.ForeignKey(to='core.Customer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderDetails',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('delivery_code', models.CharField(max_length=256, verbose_name='Delivery code')),
                ('delivery_date', models.DateField(default=django.utils.timezone.now, verbose_name='Delivery Date')),
                ('country', models.CharField(max_length=100, verbose_name='Country', blank=True)),
                ('city', models.CharField(max_length=100, verbose_name='City', blank=True)),
                ('street', models.CharField(max_length=100, verbose_name='Street', blank=True)),
                ('street_number', models.CharField(max_length=10, verbose_name='Street Number', blank=True)),
                ('flat_number', models.CharField(max_length=10, verbose_name='Flat Number', blank=True)),
                ('zip', models.CharField(max_length=15, verbose_name='Postal code', blank=True)),
                ('address_notes', models.TextField(verbose_name=b'Address notes', blank=True)),
                ('delivery_center', models.ForeignKey(related_name='order_details', to='core.CustomerDistributionCenter')),
                ('order', models.ForeignKey(related_name='details', to='core.Order')),
            ],
            options={
                'verbose_name_plural': 'Order Details',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='OrderEntry',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveIntegerField(verbose_name='Amount')),
                ('order_details', models.ForeignKey(related_name='entries', to='core.OrderDetails')),
            ],
            options={
                'verbose_name_plural': 'Order Entry',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Package',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('weight', models.DecimalField(verbose_name='Weight', max_digits=22, decimal_places=2)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PackageAmount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount', models.PositiveIntegerField(default=1, verbose_name='Amount')),
                ('container', models.ForeignKey(related_name='packages_amount', to='core.Container')),
                ('package', models.ForeignKey(related_name='packages_amount', to='core.Package')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PackageStock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(default=django.utils.timezone.now, verbose_name='Date')),
                ('amount', models.PositiveIntegerField(null=True, verbose_name='Amount', blank=True)),
                ('package', models.ForeignKey(related_name='stock', to='core.Package')),
            ],
            options={
                'verbose_name_plural': 'Package Stock',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PlantStock',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateField(default=django.utils.timezone.now, verbose_name='Date')),
                ('amount', models.FloatField(verbose_name='Meters of plants', blank=True)),
            ],
            options={
                'verbose_name_plural': 'Plant Stock',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.DecimalField(verbose_name='Price', max_digits=100, decimal_places=2)),
                ('date_from', models.DateField(default=django.utils.timezone.now, verbose_name='From date')),
                ('date_to', models.DateField(default=django.utils.timezone.now, verbose_name='To date')),
                ('customer', models.ForeignKey(to='core.Customer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('assumed_weight', models.DecimalField(verbose_name='Assumed weight', max_digits=22, decimal_places=2)),
                ('real_weight', models.DecimalField(verbose_name='Real weight', max_digits=22, decimal_places=2)),
                ('packaging', models.ForeignKey(related_name='products', to='core.PackageAmount')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProductRaceConfig',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('percent', models.DecimalField(verbose_name='Percent', max_digits=7, decimal_places=4)),
                ('product', models.ForeignKey(related_name='races_config', to='core.Product')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Race',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('avg_weight', models.DecimalField(null=True, verbose_name='Average weight', max_digits=20, decimal_places=2, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TradeClass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
            ],
            options={
                'verbose_name_plural': 'Trade Classes',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='productraceconfig',
            name='race',
            field=models.ForeignKey(related_name='+', to='core.Race'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='races',
            field=models.ManyToManyField(related_name='products', through='core.ProductRaceConfig', to='core.Race'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='product',
            name='trade_class',
            field=models.ForeignKey(related_name='+', to='core.TradeClass'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='price',
            name='product',
            field=models.ForeignKey(to='core.Product'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='plantstock',
            name='race',
            field=models.ForeignKey(related_name='stock', to='core.Race'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='package',
            name='container',
            field=models.ManyToManyField(to='core.Container', through='core.PackageAmount'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderentry',
            name='price',
            field=models.ForeignKey(to='core.Price'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='orderentry',
            name='product',
            field=models.ForeignKey(related_name='order_entries', to='core.Product'),
            preserve_default=True,
        ),
    ]
