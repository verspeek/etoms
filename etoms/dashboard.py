"""
This file was generated with the customdashboard management command and
contains the class for the main dashboard.

To activate your index dashboard add the following to your settings.py::
    GRAPPELLI_INDEX_DASHBOARD = 'etoms.dashboard.CustomIndexDashboard'
"""

from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for www.
    """

    def init_with_context(self, context):
        site_name = get_admin_site_name(context)
        request = context.get('request', None)

        if request and request.user.is_superuser:
            orders_models = ('apps.core.models.Order', 'apps.core.models.Customer')
        else:
            orders_models = ('apps.core.models.Order', )
        self.children.append(modules.ModelList(
            _("Orders"),
            column=1,
            models=orders_models,
            exclude=('django.contrib.*',),
        ))

        self.children.append(modules.ModelList(
            _("Stock & Harvest"),
            column=1,
            models=('apps.core.models.*Stock',),
            exclude=('django.contrib.*',),
        ))

        self.children.append(modules.ModelList(
            _("Products Configuration"),
            column=1,
            models=('apps.core.models.Product', 'apps.core.models.Race',
                    'apps.core.models.Package', 'apps.core.models.Container',
                    'apps.core.models.TradeClass'),
            exclude=('django.contrib.*',),
        ))

        self.children.append(modules.ModelList(
            _("Reports"),
            column=1,
            models=('apps.reports.models.Report',),
            exclude=('django.contrib.*',),
        ))

        if request and request.user.is_superuser:
            self.children.append(modules.ModelList(
                _("All Data Models"),
                column=1,
                collapsible=True,
                css_classes=('collapse grp-closed',),
                exclude=('django.contrib.*',),
            ))

        self.children.append(modules.LinkList(
            _('Reports'),
            column=2,
            children=[
                {
                    'title': _('Pick Lists'),
                    'url': '/core/order/picklist/',
                    'external': False,
                },
                {
                    'title': _('Product report'),
                    'url': '/core/orderentry/',
                    'external': False,
                },
            ]
        ))

        self.children.append(modules.ModelList(
            _("User Administration"),
            column=2,
            collapsible=True,
            css_classes=('collapse grp-closed',),
            models=('django.contrib.*',),
        ))

        # append a recent actions module
        # self.children.append(modules.RecentActions(
        #     _("Recent Actions"),
        #     column=3,
        #     limit=10,
        #     collapsible=False,
        # ))

        # append a group for "Administration" & "Applications"
        # self.children.append(modules.Group(
        #     _('Group: Administration & Applications'),
        #     column=1,
        #     collapsible=True,
        #     children = [
        #         modules.AppList(
        #             _('Administration'),
        #             column=1,
        #             collapsible=False,
        #             models=('django.contrib.*',),
        #         ),
        #         modules.AppList(
        #             _('Applications'),
        #             column=1,
        #             css_classes=('collapse closed',),
        #             exclude=('django.contrib.*',),
        #         )
        #     ]
        # ))

        # append another link list module for "support".
        # self.children.append(modules.LinkList(
        #     _('Media Management'),
        #     column=2,
        #     children=[
        #         {
        #             'title': _('FileBrowser'),
        #             'url': '/admin/filebrowser/browse/',
        #             'external': False,
        #         },
        #     ]
        # ))

        # append another link list module for "support".
        # self.children.append(modules.LinkList(
        #     _('Support'),
        #     column=2,
        #     children=[
        #         {
        #             'title': _('Django Documentation'),
        #             'url': 'http://docs.djangoproject.com/',
        #             'external': True,
        #         },
        #         {
        #             'title': _('Grappelli Documentation'),
        #             'url': 'http://packages.python.org/django-grappelli/',
        #             'external': True,
        #         },
        #         {
        #             'title': _('Grappelli Google-Code'),
        #             'url': 'http://code.google.com/p/django-grappelli/',
        #             'external': True,
        #         },
        #     ]
        # ))

        # append a feed module
        # self.children.append(modules.Feed(
        #     _('Latest Django News'),
        #     column=2,
        #     feed_url='http://www.djangoproject.com/rss/weblog/',
        #     limit=5
        # ))



