import django_filters
from rest_framework import routers, serializers, viewsets, filters
from apps.core.models import CustomerDistributionCenter, Price
router = routers.DefaultRouter()


class CustomerDistributionCenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerDistributionCenter
        exclude = ('default_products',)

    default_products = serializers.SerializerMethodField()

    def get_default_products(self, obj):
        return [p['id'] for p in obj.default_products.extra(
            select={'order_null': '"core_product"."order" is NULL'},
            order_by=['order_null', 'order', 'name']).distinct().values('id')]


class PriceSerializer(serializers.ModelSerializer):
    name = serializers.ReadOnlyField(source='__unicode__')

    class Meta:
        model = Price


class CustomerDistributionCenterViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CustomerDistributionCenterSerializer
    queryset = CustomerDistributionCenter.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ['customer']


class PriceViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = PriceSerializer
    queryset = Price.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ['period__customer', 'product', 'period__date_from', 'period__date_to']

router.register(r'customerdistributioncenter', CustomerDistributionCenterViewSet)
router.register(r'price', PriceViewSet)
