server {
    listen   80;

    server_name etoms.italamo.com
                87.233.242.162
                ;

    #@true.nl  wij willen graag toegang tot deze logfiles, waar ze staan maakt ons niet veel uit
    access_log  /var/log/nginx/etoms.italamo.com-access.log;
    error_log  /var/log/nginx/etoms.italamo.com-error.log;

    client_max_body_size 0;

    location ~ ^/static/(ref-[^/]*/)?(.*)$ {
        alias /data/www/releases/etoms/etoms/htdocs/statics/$2;
        expires 1y;
        add_header Cache-Control "public";
    }

    location / {
       include uwsgi_params;
       uwsgi_pass unix:///tmp/uwsgi-etoms.italamo.sock;
       uwsgi_param  UWSGI_SCHEME   $scheme;
       #uwsgi_read_timeout 10s;
       #client_max_body_size 3M;
    }

    # deny access to hidden files   (.svn/.git/.hg)
    #(including .htaccess files, if Apache's document root # concurs with nginx's one
    location ~ /\. {
        access_log off;
        log_not_found off;
        deny all;
    }
}


